package com.example.register.router

import com.example.register.model.UserInfoResponse
import com.example.register.service.UserService
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*

@Component
class UserInfoHandler(
    private val userService: UserService
) {

    suspend fun demo(request: ServerRequest): ServerResponse {
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait("OK")
    }

    suspend fun findOne(request: ServerRequest): ServerResponse {
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(userService.findUserInfo(request.pathVariable("id"))!!)
    }

    suspend fun finds(request: ServerRequest): ServerResponse {
        return userService.findAll().let {
            ServerResponse
                .ok()
                .json()
                .bodyValueAndAwait(it)
        }
    }

    suspend fun register(request: ServerRequest): ServerResponse {
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(UserInfoResponse(userService.register(request.awaitBody())))
    }

    suspend fun topUp(request: ServerRequest): ServerResponse {
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(UserInfoResponse(userService.topUp(request.awaitBody())))
    }
}
