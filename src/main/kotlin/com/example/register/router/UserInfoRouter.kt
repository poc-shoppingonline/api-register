package com.example.register.router

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.coRouter
import org.springframework.web.reactive.function.server.json

@Configuration
class UserInfoRouter {
    @Bean
    fun router(userInfoHandler: UserInfoHandler) = coRouter {
        accept(MediaType.APPLICATION_JSON).nest {
            GET("/user/{id}", userInfoHandler::findOne)
            GET("/users", userInfoHandler::finds)
            POST("/user/register", userInfoHandler::register)
            POST("/user/top-up", userInfoHandler::topUp)

            GET("/demo", userInfoHandler::demo)
        }
    }

}

@Configuration
class RouteConfigurations {
    @Bean
    fun mainRouter() = coRouter {
        GET("/demo") {
            ServerResponse
                .ok()
                .json()
                .bodyValueAndAwait("OK")
        }
    }
}