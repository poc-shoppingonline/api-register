package com.example.register.model

data class UserInfoRequest(
    val userId: String,
    val balance: Int
)

data class UserInfoResponse(
    val message: String
)