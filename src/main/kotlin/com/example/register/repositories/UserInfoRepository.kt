package com.example.register.repositories

import com.example.register.entities.UserInfo
import org.springframework.data.r2dbc.core.*
import org.springframework.data.relational.core.query.Criteria
import org.springframework.stereotype.Repository

@Repository
class UserInfoRepository(
    private val databaseClient: DatabaseClient
) {
    suspend fun finds() =
        databaseClient.select().from(UserInfo::class.java)
            .fetch().flow()

    suspend fun find(userID: String) =
        databaseClient.select().from(UserInfo::class.java)
            .matching(Criteria.where(UserInfo::userId.name).`is`(userID))
            .fetch()
            .awaitFirstOrNull()

    suspend fun insert(userInfo: UserInfo) {
        databaseClient.insert()
            .into(UserInfo::class.java)
            .using(userInfo).await()
    }

    suspend fun update(userInfo: UserInfo) {
        databaseClient.update()
            .table(UserInfo::class.java)
            .using(userInfo)
            .fetch().awaitRowsUpdated()
    }
}