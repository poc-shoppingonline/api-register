package com.example.register.entities

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("USER_INFO")
data class UserInfo(
    @Id
    val userId: String,
    val balance: Int
)