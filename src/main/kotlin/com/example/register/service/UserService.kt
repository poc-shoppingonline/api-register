package com.example.register.service

import com.example.register.entities.UserInfo
import com.example.register.model.UserInfoRequest
import com.example.register.repositories.UserInfoRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.toList
import org.springframework.stereotype.Service

@Service
class UserService(
    private val userInfoRepository: UserInfoRepository
) {

    suspend fun findUserInfo(id: String) = userInfoRepository.find(id)
    suspend fun findAll() = userInfoRepository.finds().toList()

    suspend fun register(userInfoRequest: UserInfoRequest) =
        userInfoRepository.insert(UserInfo(userInfoRequest.userId, userInfoRequest.balance)).let {
            "register success"
        }
//        userInfoRepository.find(userInfoRequest.userId)?.let {
//            userInfoRepository.insert(UserInfo(userInfoRequest.userId, userInfoRequest.balance))
//            "register success"
//        } ?: "User is exist"

    suspend fun topUp(userInfoRequest: UserInfoRequest) =
        userInfoRepository.update(UserInfo(userInfoRequest.userId, userInfoRequest.balance)).let {
            "Top Up Success"
        }
//        userInfoRepository.find(userInfoRequest.userId)?.let {
//            userInfoRepository.update(UserInfo(userInfoRequest.userId, userInfoRequest.balance))
//            "Top Up Success"
//        } ?: "User is find not found"
}